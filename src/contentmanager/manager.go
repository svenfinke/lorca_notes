package contentmanager

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"

	"github.com/zserge/lorca"
	"gitlab.com/svenfinke/lorca_notes/assets"
)

// ContentManager is used to manage and render the content of the window
type ContentManager struct {
	CurrentPage string
	ui          lorca.UI
	events      []uiEvent
}

// UiEvent is a struct that is used to bind events to the UI
type uiEvent struct {
	Eventname string
	Callable  func()
}

// New returns an instance of the ContentManager
func New(ui lorca.UI) ContentManager {
	cntmgr := ContentManager{"/", ui, []uiEvent{}}

	return cntmgr
}

// BindEvent binds an Event to the ui
func (cntmgr *ContentManager) BindEvent(eventName string, callable func()) {
	cntmgr.events = append(cntmgr.events, uiEvent{eventName, callable})
}

// Init will initialize the ContentManager and Bind all defined Events
func (cntmgr *ContentManager) Init() {
	// A simple way to know when UI is ready (uses body.onload event in JS)
	for _, event := range cntmgr.events {
		cntmgr.ui.Bind(event.Eventname, event.Callable)
	}
}

func (cntmgr *ContentManager) Run() {
	// Load HTML.
	// You may also use `data:text/html,<base64>` approach to load initial HTML,
	// e.g: ui.Load("data:text/html," + url.PathEscape(html))

	ln, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		log.Fatal(err)
	}
	defer ln.Close()
	go http.Serve(ln, http.FileServer(assets.FS))
	cntmgr.ui.Load(fmt.Sprintf("http://%s", ln.Addr()))

	// Wait until the interrupt signal arrives or browser window is closed
	sigc := make(chan os.Signal)
	signal.Notify(sigc, os.Interrupt)
	select {
	case <-sigc:
	case <-cntmgr.ui.Done():
	}

	log.Println("exiting...")
}
