package page

import (
	"github.com/zserge/lorca"
)

// Page is defining the basic interface to Render a Page
type Page interface {
	Render(ui lorca.UI)
	GetEventName() string
}
