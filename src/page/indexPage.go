package page

import (
	"encoding/json"
	"fmt"

	"github.com/zserge/lorca"
)

type IndexPage struct {
	Ui lorca.UI
}

// Render is providing Data to render the index page
func (p *IndexPage) Render() {
	data, _ := json.Marshal(getNotes())
	p.Ui.Eval(fmt.Sprintf("render('%s')", data))
}

func (p *IndexPage) GetEventName() string {
	return "renderIndexPage"
}

type Note struct {
	Title   string
	Content string
}

func getNotes() []Note {
	notes := []Note{}

	notes = append(notes, Note{"FooBar", "Awesome Content"})
	notes = append(notes, Note{"FooBar 22", "Awesome Content"})
	notes = append(notes, Note{"FooBar 22", "Awesome Content BAR"})

	return notes
}
