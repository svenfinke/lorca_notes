function NoteList(notes){
    this._notes = notes;
}
NoteList.prototype = {
    _notes : [],
    addNote: function(note){
        this._notes.push(note);
    },
    render: function(target){
        this._notes.forEach(function(note){
            note.render(target);
        });
    }
}

function Note(name, content, created_at){
    this.name = name;
    this.content = content;
    this.created_at = created_at;
}
Note.prototype = {
    name: "",
    content: "",
    created_at: "",
    render: function(target){
        console.log(target, this);
    }
}

notes = new NoteList([
    new Note('Hello', '#World'),
    new Note('Hello', '#FooBar')
])

