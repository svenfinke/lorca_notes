package main

import (
	"log"
	"runtime"

	"gitlab.com/svenfinke/lorca_notes/src/contentmanager"
	"gitlab.com/svenfinke/lorca_notes/src/page"

	"github.com/zserge/lorca"
)

func main() {
	args := []string{}
	if runtime.GOOS == "linux" {
		args = append(args, "--class=Lorca")
	}
	ui, err := lorca.New("", "", 480, 320, args...)
	if err != nil {
		log.Fatal(err)
	}
	defer ui.Close()

	cntmgr := contentmanager.New(ui)

	indexPage := page.IndexPage{ui}
	cntmgr.BindEvent(indexPage.GetEventName(), indexPage.Render)

	cntmgr.Init()
	cntmgr.Run()
}
